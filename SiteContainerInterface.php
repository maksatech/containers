<?php
namespace Maksatech\Containers;

/**
 * Interface SiteContainerInterface
 * @package Maksatech\Containers
 */
interface SiteContainerInterface extends BaseContainerInterface
{
    /**
     * @param SiteInterface|null $site
     * @return void
     */
    public function setSite(SiteInterface $site): void;

    /**
     * @return null|SiteInterface
     */
    public function getSite();

    /**
     * @return bool
     */
    public function hasSite(): bool;
}