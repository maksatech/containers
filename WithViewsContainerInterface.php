<?php
namespace Maksatech\Containers;

/**
 * Interface WithViewsContainerInterface
 * @package Maksatech\Containers
 */
interface WithViewsContainerInterface
{
    /**
     * @param ViewsContainerInterface|null $viewsContainer
     * @return mixed
     */
    public function setViewsContainer(ViewsContainerInterface $viewsContainer): void;

    /**
     * @return ViewsContainerInterface|null
     */
    public function getViewsContainer();

    /**
     * @return bool
     */
    public function hasViewsContainer(): bool;
}