<?php
namespace Maksatech\Containers;

/**
 * Interface InstancesContainerInterface
 * @package Maksatech\Containers
 */
interface InstancesContainerInterface extends BaseContainerInterface
{
    /**
     * @param string $className
     * @return object
     */
    public function getInstanceOf(string $className);

    /**
     * @param string $className
     * @param object $object
     * @return void
     */
    public function setInstanceOf(string $className, object $object);

    /**
     * @param string $className
     * @return bool
     */
    public function hasInstanceOf(string $className): bool;
}