<?php
namespace Maksatech\Containers;

/**
 * Interface LanguageInterface
 * @package Maksatech\Containers
 */
interface LanguageInterface
{
    /**
     * @return mixed
     */
    public function getKey();

    /**
     * @return ?LanguageInterface
     */
    public static function loadDefault(): ?LanguageInterface;

    /**
     * @param string $uri
     * @param bool $ignoreDefault
     * @return ?LanguageInterface
     */
    public static function loadByUri(string $uri, bool $ignoreDefault = true): ?LanguageInterface;

    /**
     * @return LanguageInterface[]
     */
    public static function loadList(): array;

    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @return string
     */
    public function getUri(): string;

    /**
     * @return bool
     */
    public function isDefault(): bool;
}