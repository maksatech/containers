<?php
namespace Maksatech\Containers;

/**
 * Interface WithInstancesContainerInterface
 * @package Maksatech\Containers
 */
interface WithInstancesContainerInterface
{
    /**
     * @param InstancesContainerInterface|null $instancesContainer
     * @return void
     */
    public function setInstancesContainer(InstancesContainerInterface $instancesContainer): void;

    /**
     * @return InstancesContainerInterface|null
     */
    public function getInstancesContainer();

    /**
     * @return bool
     */
    public function hasInstancesContainer(): bool;
}