<?php
namespace Maksatech\Containers;

use Illuminate\Translation\Translator;
use Maksatech\Containers\Exceptions\LanguageNullException;

/**
 * Interface LocaleContainerInterface
 * @package Maksatech\Containers
 */
interface LocaleContainerInterface extends BaseContainerInterface
{
    /**
     * Get the Translator object by locale string identifier
     *
     * @param string $locale
     * @param string $group
     * @return Translator
     */
    public function getTranslatorByLocale(string $locale, string $group): Translator;

    /**
     * @param LanguageInterface $language
     * @return void
     */
    public function setLanguage(LanguageInterface $language): void;

    /**
     * @return LanguageInterface
     */
    public function getLanguage();

    /**
     * @return bool
     */
    public function hasLanguage(): bool;

    /**
     * @param string $key
     * @param array $parameters
     * @param string $group
     * @return string
     * @throws LanguageNullException
     */
    public function trans(string $key, array $parameters = [], string $group = 'app'): string;
}