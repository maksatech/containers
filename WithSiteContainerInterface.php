<?php
namespace Maksatech\Containers;

/**
 * Interface WithSiteContainerInterface
 * @package Maksatech\Containers
 */
interface WithSiteContainerInterface
{
    /**
     * @param SiteContainerInterface|null $siteContainer
     * @return void
     */
    public function setSiteContainer(SiteContainerInterface $siteContainer): void;

    /**
     * @return SiteContainerInterface|null
     */
    public function getSiteContainer();

    /**
     * @return bool
     */
    public function hasSiteContainer(): bool;
}