<?php
namespace Maksatech\Containers;

/**
 * Interface WithRequestContainerInterface
 * @package Maksatech\Containers
 */
interface WithRequestContainerInterface
{
    /**
     * @param RequestContainerInterface|null $requestContainer
     * @return void
     */
    public function setRequestContainer(RequestContainerInterface $requestContainer): void;

    /**
     * @return RequestContainerInterface|null
     */
    public function getRequestContainer();

    /**
     * @return bool
     */
    public function hasRequestContainer(): bool;
}