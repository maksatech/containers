<?php
namespace Maksatech\Containers;

use Illuminate\Database\Capsule\Manager;

/**
 * Interface DatabaseContainerInterface
 * @package Maksatech\Containers
 */
interface DatabaseContainerInterface extends BaseContainerInterface
{
    /**
     * @param null|Manager $capsule
     * @return void
     */
    public function setDatabaseCapsule(Manager $capsule): void;

    /**
     * @return null|Manager
     */
    public function getDatabaseCapsule();

    /**
     * @return bool
     */
    public function hasDatabaseCapsule(): bool;
}