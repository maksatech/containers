<?php
namespace Maksatech\Containers;

/**
 * Interface WithLocaleContainerInterface
 * @package Maksatech\Containers
 */
interface WithLocaleContainerInterface
{
    /**
     * @param LocaleContainerInterface|null $localeContainer
     * @return void
     */
    public function setLocaleContainer(LocaleContainerInterface $localeContainer): void;

    /**
     * @return LocaleContainerInterface|null
     */
    public function getLocaleContainer();

    /**
     * @return bool
     */
    public function hasLocaleContainer(): bool;
}