<?php
namespace Maksatech\Containers;

/**
 * Interface WithDatabaseContainerInterface
 * @package Maksatech\Containers
 */
interface WithDatabaseContainerInterface
{
    /**
     * @param DatabaseContainerInterface|null $databaseContainer
     * @return void
     */
    public function setDatabaseContainer(DatabaseContainerInterface $databaseContainer): void;

    /**
     * @return DatabaseContainerInterface|null
     */
    public function getDatabaseContainer();

    /**
     * @return bool
     */
    public function hasDatabaseContainer(): bool;
}