<?php
namespace Maksatech\Containers;

/**
 * Interface BaseContainerInterface
 * @package Maksatech\Containers
 */
interface BaseContainerInterface
{
    /**
     * @return string
     */
    public function env(): string;
}