<?php
namespace Maksatech\Containers;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Http\Request as IlluminateHttpRequest;

/**
 * Interface RequestContainerInterface
 * @package Maksatech\Containers
 */
interface RequestContainerInterface extends BaseContainerInterface
{
    /**
     * @param null|Request|FormRequest|IlluminateHttpRequest $request
     * @return void
     */
    public function setRequest($request): void;

    /**
     * @return null|Request|FormRequest|IlluminateHttpRequest
     */
    public function getRequest();

    /**
     * @return bool
     */
    public function hasRequest(): bool;
}