<?php
namespace Maksatech\Containers;

/**
 * Interface SiteInterface
 * @package Maksatech\Containers
 */
interface SiteInterface
{
    /**
     * @param string $domain
     * @return SiteInterface|null
     */
    public static function loadByDomain(string $domain);
}