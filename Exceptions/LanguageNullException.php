<?php
namespace Maksatech\Containers\Exceptions;

use Exception;
use Throwable;

/**
 * Class LanguageNullException
 * @package Maksatech\Containers\Exceptions
 */
class LanguageNullException extends Exception
{
    /**
     * LanguageNullException constructor.
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct('Language is null', $code, $previous);
    }
}